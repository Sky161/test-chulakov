module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends": "airbnb",
    "parser": "babel-eslint",
     "rules": {
        "no-console": "warn",
        "indent": [2, "tab", { "SwitchCase": 1, "VariableDeclarator": 1 }],
        "no-tabs": 0,
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        "react/prop-types": 0,
        "react/jsx-indent": [2, "tab"],
        "react/jsx-indent-props": [2, "tab"],
        "comma-dangle": ["error", "never"],
        "jsx-quotes": [2, "prefer-single"],
        "import/no-unresolved": [2, { ignore: ['@.*$'] }],
        "import/extensions": 0,
        "no-shadow": 0,
        "import/prefer-default-export": 0,
        "max-len": [2, 140]
     },
};
