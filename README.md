# Requirements #

* Node JS
* npm or yarn

# How to start #

1. `git clone https://bitbucket.org/Sky161/test-chulakov`
2. `cd test-chulakov`
3. `npm i` or `yarn`
4. `npm run build` or `yarn run build`

# How start dev server and hot reload #

`npm start` or `yarn start`

## Builds files in directory "build" ##