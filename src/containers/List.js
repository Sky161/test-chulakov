import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';

// import actions
import { getPeoplesList, toggleFavorite, getFromStorageList } from '@/actions/peoples';

// import views
import TableView from '@/components/Table/';
import ListView from '@/components/List/';

// import helpers
import parseSearchParamsUrl from '@/helpers/parseSearchParamsUrl.js';

class ListContainer extends Component {
	static propTypes = {
		getPeoplesList: PropTypes.func.isRequired,
		peoples: ImmutableProptypes.list.isRequired,
		currentLang: PropTypes.string.isRequired,
		match: PropTypes.shape({
			params: PropTypes.objectOf(PropTypes.string).isRequired
		}).isRequired,
		toggleFavorite: PropTypes.func.isRequired,
		getFromStorageList: PropTypes.func.isRequired,
		sortingButtons: ImmutableProptypes.map.isRequired
	}

	constructor(props) {
		super(props);
		this.state = {
			peoples: props.peoples
		};
	}

	componentWillMount() {
		const { getPeoplesList, getFromStorageList } = this.props;
		if (localStorage.getItem('list')) {
			getFromStorageList();
		} else {
			getPeoplesList();
		}
	}

	componentWillReceiveProps(nextProps) {
		const { peoples, location } = nextProps;
		const objSearchquery = parseSearchParamsUrl(location);

		if (Object.prototype.hasOwnProperty.call(objSearchquery, 'filter')) {
			this.setState({
				peoples: peoples.filter(it => it.get('name').toLowerCase().indexOf(objSearchquery.filter.toLowerCase()) !== -1)
			});
		} else {
			this.setState({ peoples });
		}
	}

	render() {
		const { currentLang, match, toggleFavorite, sortingButtons } = this.props;
		const { peoples } = this.state;

		if (match.params.view === 'table') {
			return (<TableView
				peoples={peoples}
				currentLang={currentLang}
				toggleFavorite={toggleFavorite}
				sortingButtons={sortingButtons}
			/>);
		}
		return <ListView peoples={peoples} currentLang={currentLang} toggleFavorite={toggleFavorite} sortingButtons={sortingButtons} />;
	}
}

export default connect(
	state => ({
		peoples: state.peoples.get('items'),
		currentLang: state.app.get('lang'),
		sortingButtons: state.peoples.get('sortingButtons')
	}),
	{
		getPeoplesList,
		toggleFavorite,
		getFromStorageList
	}
)(ListContainer);
