import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

// import lang json
import lang from '@/lang.json';

// import view
import NavLinkGenerator from '@/components/NavLinkGenerator';

const ToggleContainer = ({ currentLang }) => {
	const linkList = [
		{
			to: 'table',
			name: lang.table[currentLang]
		},
		{
			to: 'preview',
			name: lang.preview[currentLang]
		}
	];

	return (
		<div className='toggle-view'>
			<div className='btn-group'>
				<NavLinkGenerator linkList={linkList} idx={1} />
			</div>
		</div>
	);
};

ToggleContainer.propTypes = {
	currentLang: PropTypes.string.isRequired
};

export default withRouter(connect(
	state => ({
		currentLang: state.app.get('lang')
	})
)(ToggleContainer));
