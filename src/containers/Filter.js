import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// import styles
import '@/components/Filter/style.sass';

// import lang json
import lang from '@/lang.json';

// import helpers
import parseSearchParamsUrl from '@/helpers/parseSearchParamsUrl.js';

class FilterContainer extends Component {
	static propTypes = {
		currentLang: PropTypes.string.isRequired
	}

	constructor(props) {
		super(props);
		this.state = {
			query: ''
		};
	}

	componentWillMount() {
		const objSearch = parseSearchParamsUrl(location);
		if (Object.prototype.hasOwnProperty.call(objSearch, 'filter')) {
			this.setState({ query: objSearch.filter });
		}
	}

	handleChange = (e) => {
		const { history } = this.props;
		const val = e.target.value;
		if (val) {
			history.push({ search: `?filter=${val}` });
		} else {
			history.push({ search: '' });
		}
		this.setState({ query: val });
	}

	render() {
		const { currentLang } = this.props;

		return (
			<div className='filter'>
				<input
					type='text'
					name='filter'
					className='filter__input'
					placeholder={lang.search[currentLang]}
					onChange={this.handleChange}
					value={this.state.query}
				/>
			</div>
		);
	}
}

export default connect(
	state => ({
		currentLang: state.app.get('lang')
	})
)(FilterContainer);
