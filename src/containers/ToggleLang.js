import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// import views
import ButtonGenerator from '@/components/ButtonGenerator';

// import actions
import { toggleLang } from '@/actions/app';

const buttonArray = [
	{
		name: 'RU',
		value: 'RU'
	},
	{
		name: 'EN',
		value: 'EN'
	}
];

const ToggleLangContainer = ({ currentLang, toggleLang }) => (
	<ButtonGenerator arrayButton={buttonArray} activeButton={currentLang} handleClick={toggleLang} />
);

ToggleLangContainer.propTypes = {
	currentLang: PropTypes.string.isRequired,
	toggleLang: PropTypes.func.isRequired
};

export default connect(
	state => ({
		currentLang: state.app.get('lang')
	}),
	{
		toggleLang
	}
)(ToggleLangContainer);
