import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// import actions
import { sortList, sortListDirection } from '@/actions/peoples.js';

// import views
import NavLinkGenerator from '@/components/NavLinkGenerator.js';

// import lang json
import lang from '@/lang.json';

class SortContainer extends Component {
	static propTypes = {
		sortList: PropTypes.func.isRequired,
		sortListDirection: PropTypes.func.isRequired,
		currentLang: PropTypes.string.isRequired,
		match: PropTypes.shape({
			params: PropTypes.objectOf(PropTypes.string)
		}).isRequired
	}

	componentWillReceiveProps(nextProps) {
		const { match, sortList, sortListDirection } = nextProps;
		if (match.params.sorting !== this.props.match.params.sorting) {
			sortList(match.params.sorting);
		}
		if (match.params.direction !== this.props.match.params.direction) {
			sortListDirection(match.params.direction);
		}
	}

	render() {
		const { currentLang } = this.props;
		const buttonsSort = [
			{
				name: 'ID',
				to: 'id'
			},
			{
				name: lang.name[currentLang],
				to: 'name'
			},
			{
				name: lang.age[currentLang],
				to: 'age'
			}
		];

		const directionSort = [
			{
				name: lang.ascending[currentLang],
				to: 'up'
			},
			{
				name: lang.descendingly[currentLang],
				to: 'down'
			}
		];

		return (
			<div className='sorting'>
				<div className='btn-sorting'>
					<NavLinkGenerator linkList={buttonsSort} idx={2} />
				</div>
				<NavLinkGenerator linkList={directionSort} idx={3} />
			</div>
		);
	}
}

export default connect(
	state => ({
		currentLang: state.app.get('lang')
	}),
	{
		sortList,
		sortListDirection
	}
)(SortContainer);
