import React, { Component } from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import { Player } from 'video-react';
import PropTypes from 'prop-types';

// import lang json
import lang from '@/lang.json';

// import styles
import './style.sass';

class ListView extends Component {
	static propTypes = {
		peoples: ImmutableProptypes.list.isRequired,
		currentLang: PropTypes.string.isRequired,
		toggleFavorite: PropTypes.func.isRequired,
		sortingButtons: ImmutableProptypes.map.isRequired
	}

	constructor(props) {
		super(props);
		this.lastShownItem = -1;
		this.refsArr = [];
	}

	componentDidMount() {
		clearTimeout(this.timer);
		this.timer = setTimeout(this.handleShowingItem);
		window.addEventListener('resize', this.handleShowingItem);
		window.addEventListener('scroll', this.handleShowingItem);
	}

	componentWillReceiveProps(nextProps) {
		if (!nextProps.sortingButtons.equals(this.props.sortingButtons)) {
			this.refsArr.forEach((item) => {
				if (item) {
					item.classList.remove('list__item_visible');
				}
			});
			this.lastShownItem = -1;
			this.refsArr = [];

			clearTimeout(this.timer);
			this.timer = setTimeout(this.handleShowingItem);
		}
	}

	componentWillUnmount() {
		clearTimeout(this.timer);
		window.removeEventListener('resize', this.handleShowingItem);
		window.removeEventListener('scroll', this.handleShowingItem);
	}

	handleShowingItem = () => {
		if (this.lastShownItem >= this.refsArr.length - 1 || this.refsArr.length === 0) return;
		this.lastShownItem = this.lastShownItem + 1;
		if (this.refsArr[this.lastShownItem] && this.refsArr[this.lastShownItem].getBoundingClientRect().top < window.innerHeight) {
			this.refsArr[this.lastShownItem].classList.add('list__item_visible');
		} else {
			this.lastShownItem = this.lastShownItem - 1;
		}
	}

	storeRef = idx => (ref) => {
		this.refsArr[idx] = ref;
	}

	render() {
		const { peoples, currentLang, toggleFavorite } = this.props;
		return (
			<div className='list row'>
				{peoples.map((item, idx) => (
					<article
						className={`
							list__item ${item.get('video') ? 'list__item_full-width' : ''}
							${idx <= this.lastShownItem ? 'list__item_visible' : ''}
							col
						`}
						key={item.get('id')}
						onTransitionEnd={this.handleShowingItem}
						ref={this.storeRef(idx)}
					>
						<div className='row'>
							<section className='list__card col'>
								<div className='list__item-header row'>
									<div className='list__img col'>
										<img
											alt={item.get('image')}
											src={`${process.env.PUBLIC_URL}/images/${item.get('image')}.svg`}
										/>
									</div>
									<span className='col'>{item.get('name')}</span>
									<i
										className={`col fa list__favorite ${item.get('favourite') ? 'fa-star' : 'fa-star-o'}`}
										aria-hidden='true'
										onClick={() => toggleFavorite(item.get('id'))}
									/>
								</div>
								<p className='list__age'>{item.get('age')} {lang.years[currentLang]}</p>
								<p className='list__phone'>{item.get('phone')}</p>
								<div className='list__text'>{item.get('phrase')}</div>
							</section>
							{item.get('video') &&
								<section className='list__video col'>
									<Player src={`${process.env.PUBLIC_URL}/videos/${item.get('video')}.mp4`} playsInline />
								</section>
							}
						</div>
					</article>
				))}
			</div>
		);
	}
}

export default ListView;
