import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => (
	<div className='not-found'>
		<h1>Уууупс вы все сломали! :(</h1>
		<p><Link to='/'>Вернитесь лучше на главную, пока никто не заметил ;)</Link></p>
	</div>
);

export default NotFound;
