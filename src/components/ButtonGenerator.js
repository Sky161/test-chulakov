import React from 'react';
import PropTypes from 'prop-types';

const ButtonGenerator = ({ arrayButton, activeButton, handleClick }) => (
	<div className='btn-group'>
		{arrayButton.map(item => (
			<button
				key={item.value}
				className={`btn ${item.value === activeButton ? 'btn_active' : ''}`}
				onClick={() => handleClick(item.value)}
			>
				{item.name}
			</button>
		))}
	</div>
);

ButtonGenerator.propTypes = {
	arrayButton: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string.isRequired,
			value: PropTypes.string.isRequired
		}).isRequired
	).isRequired,
	activeButton: PropTypes.string.isRequired,
	handleClick: PropTypes.func.isRequired
};

export default ButtonGenerator;
