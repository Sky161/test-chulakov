import React from 'react';
import PropTypes from 'prop-types';

// import containers
import ToggleViewContainer from '@/containers/ToggleView';
import ListContainer from '@/containers/List';
import SortContainer from '@/containers/Sort';
import FilterContainer from '@/containers/Filter';
import ToggleLangContainer from '@/containers/ToggleLang';

const LayotView = ({ match, history, location }) => (
	<div className='container'>
		<header className='header'>
			<div className='row'>
				<div className='col'><SortContainer match={match} /></div>
				<div className='col text-right'>
					<ToggleViewContainer />
					<ToggleLangContainer />
				</div>
			</div>
			<FilterContainer history={history} location={location} />
		</header>
		<ListContainer match={match} location={location} />
	</div>
);

LayotView.propTypes = {
	match: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.object, PropTypes.string, PropTypes.bool])).isRequired,
	history: PropTypes.objectOf(PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.func,
		PropTypes.number,
		PropTypes.object
	])).isRequired
};

export default LayotView;
