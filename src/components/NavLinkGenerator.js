import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { fromJS } from 'immutable';
import PropTypes from 'prop-types';

const NavLinkGenerator = ({ location, linkList, idx }) => {
	const locationList = fromJS(location.pathname.split('/'));

	return (
		<div className='btn-group'>
			{linkList.map(item => (
				<NavLink
					key={item.to}
					to={locationList.set(idx, item.to).toJS().join('/')}
					className='btn'
					activeClassName='btn_active'
				>
					{item.name}
				</NavLink>
			))}
		</div>
	);
};

NavLinkGenerator.propTypes = {
	location: PropTypes.objectOf(PropTypes.string).isRequired,
	linkList: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string.isRequired).isRequired).isRequired,
	idx: PropTypes.number.isRequired
};

export default withRouter(NavLinkGenerator);
