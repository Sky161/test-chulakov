import React, { Component } from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';

// import lang json
import lang from '@/lang.json';

// import styles
import './style.sass';

export default class TableView extends Component {
	static propTypes = {
		peoples: ImmutableProptypes.list.isRequired,
		currentLang: PropTypes.string.isRequired,
		toggleFavorite: PropTypes.func.isRequired,
		sortingButtons: ImmutableProptypes.map.isRequired
	}

	constructor(props) {
		super(props);
		this.lastShownItem = -1;
		this.refsArr = [];
	}

	componentDidMount() {
		clearTimeout(this.timer);
		this.timer = setTimeout(this.handleShowingItem);

		window.addEventListener('resize', this.handleShowingItem);
		window.addEventListener('scroll', this.handleShowingItem);
	}

	componentWillReceiveProps(nextProps) {
		if (!nextProps.sortingButtons.equals(this.props.sortingButtons)) {
			this.refsArr.forEach((item) => {
				if (item) {
					item.classList.remove('table__tr_visible');
				}
			});
			this.lastShownItem = -1;
			this.refsArr = [];

			clearTimeout(this.timer);
			this.timer = setTimeout(this.handleShowingItem);
		}
	}

	componentWillUnmount() {
		clearTimeout(this.timer);
		window.removeEventListener('resize', this.handleShowingItem);
		window.removeEventListener('scroll', this.handleShowingItem);
	}

	handleShowingItem = () => {
		if (this.lastShownItem >= this.refsArr.length - 1 || this.refsArr.length === 0) return;
		this.lastShownItem = this.lastShownItem + 1;
		if (this.refsArr[this.lastShownItem] && this.refsArr[this.lastShownItem].getBoundingClientRect().top < window.innerHeight) {
			this.refsArr[this.lastShownItem].classList.add('table__tr_visible');
		} else {
			this.lastShownItem = this.lastShownItem - 1;
		}
	}

	storeRef = idx => (ref) => {
		this.refsArr[idx] = ref;
	}

	render() {
		const { peoples, currentLang, toggleFavorite } = this.props;

		return (
			<table className='table'>
				<thead className='table__thead'>
					<tr>
						<th className='table__td_hide-mobile-m' />
						<th>{lang.name[currentLang]}</th>
						<th className='table__td_hide-mobile-l'>{lang.age[currentLang]}</th>
						<th>{lang.phone[currentLang]}</th>
						<th />
					</tr>
				</thead>
				<tbody>
					{peoples.map((item, idx) => (
						<tr
							key={item.get('id')}
							className={`table__tr ${idx <= this.lastShownItem ? 'table__tr_visible' : ''}`}
							ref={this.storeRef(idx)}
							onTransitionEnd={this.handleShowingItem}
						>
							<td className='table__td table__td_hide-mobile-m'>
								<img
									className='table__img'
									alt={item.get('image')}
									src={`${process.env.PUBLIC_URL}/images/${item.get('image')}.svg`}
								/>
							</td>
							<td className='table__td'>{item.get('name')}</td>
							<td className='table__td table__td_hide-mobile-l'>{item.get('age')} {lang.years[currentLang]}</td>
							<td className='table__td'>{item.get('phone')}</td>
							<td className='table__td'>
								<i
									className={`fa table__favorite ${item.get('favourite') ? 'fa-star' : 'fa-star-o'}`}
									aria-hidden='true'
									onClick={() => toggleFavorite(item.get('id'))}
								/>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		);
	}
}
