import { combineReducers } from 'redux';

// import reducers
import peoples from './peoples';
import app from './app';

export default combineReducers({
	peoples,
	app
});
