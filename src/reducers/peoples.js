import { fromJS } from 'immutable';

import {
	GET_LIST_PEOPLES_REQUEST,
	GET_LIST_PEOPLES_SUCCESS,
	GET_LIST_PEOPLES_FAIL,
	SORT_LIST,
	SORT_LIST_DIRECTION,
	TOGGLE_FAVORITE,
	GET_LOCALSTORAGE_LIST
} from '../constatns/peoples';

const initState = fromJS({
	loading: false,
	items: [],
	errors: [],
	sortingButtons: {
		sort: 'id',
		direction: 'up'
	},
	lastFavorite: null
});

export default (state = initState, action) => {
	switch (action.type) {
		case GET_LIST_PEOPLES_REQUEST:
			return state.set('loading', true);
		case GET_LIST_PEOPLES_SUCCESS:
			return state.merge({ loading: false, items: action.payload });
		case GET_LIST_PEOPLES_FAIL:
			return state.merge({ loading: false, errors: action.payload });
		case SORT_LIST:
			return state.withMutations((mutate) => {
				mutate
					.setIn(['sortingButtons', 'sort'], action.payload)
					.set('items', mutate.get('items').sort((a, b) => {
						if (a.get(action.payload) < b.get(action.payload)) return -1;
						if (a.get(action.payload) > b.get(action.payload)) return 1;
						return 0;
					}));
				if (mutate.getIn(['sortingButtons', 'direction']) === 'down') {
					mutate.set('items', mutate.get('items').reverse());
				}
			});
		case SORT_LIST_DIRECTION:
			return state.withMutations((mutate) => {
				mutate
					.setIn(['sortingButtons', 'direction'], action.payload)
					.set('items', mutate.get('items').reverse());
			});
		case TOGGLE_FAVORITE: {
			const newItems = state.get('items').map((item) => {
				if (action.payload === item.get('id')) {
					return item.set('favourite', !item.get('favourite'));
				}
				return item;
			});
			localStorage.setItem('list', JSON.stringify(newItems.toJSON()));
			return state.set('items', newItems).set('lastFavorite', action.payload);
		}
		case GET_LOCALSTORAGE_LIST: {
			const storageItems = fromJS(JSON.parse(localStorage.getItem('list')));
			return state.set('items', storageItems);
		}
		default:
			return state;
	}
};
