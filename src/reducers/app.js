import { fromJS } from 'immutable';

import {
	CHANGE_LANG
} from '../constatns/app.js';

const initState = fromJS({
	lang: 'RU'
});

export default (state = initState, action) => {
	switch (action.type) {
		case CHANGE_LANG:
			return state.set('lang', action.payload);
		default:
			return state;
	}
};
