import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';

// import root reducer
import rootReducer from './reducers/';

/* eslint-disable no-underscore-dangle */
const reduxDevtools = process.env.NODE_ENV === 'development' ?
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() : '';

export default createStore(
	rootReducer,
	reduxDevtools,
	applyMiddleware(reduxThunk)
);
/* eslint-enable */
