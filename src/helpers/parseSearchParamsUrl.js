const getObjectForUriSearch = (location) => {
	const res = {};
	location.search.substring(1).split('&').forEach((item) => {
		const itemArr = item.split('=');
		res[itemArr[0]] = decodeURI(itemArr[1]);
	});
	return res;
};

export default getObjectForUriSearch;
