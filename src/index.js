import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';

// import styles
import './index.sass';

// import store
import store from './store';

// import view
import LayotView from './components/Layot';
import NotFound from './components/NotFound';

const App = () => (
	<Provider store={store}>
		<Router>
			<Switch>
				<Route exact path='/' render={() => <Redirect to='/table/id/up' />} />
				<Route path='/:view/:sorting/:direction' component={LayotView} />
				<Route component={NotFound} />
			</Switch>
		</Router>
	</Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));
