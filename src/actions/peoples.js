import axios from 'axios';

import {
	GET_LIST_PEOPLES_REQUEST,
	GET_LIST_PEOPLES_SUCCESS,
	GET_LIST_PEOPLES_FAIL,
	SORT_LIST,
	SORT_LIST_DIRECTION,
	TOGGLE_FAVORITE,
	GET_LOCALSTORAGE_LIST
} from '../constatns/peoples';

export const getPeoplesList = () => dispatch => (
	Promise.resolve()
		.then(() => dispatch({ type: GET_LIST_PEOPLES_REQUEST }))
		.then(() => axios.get(`${process.env.PUBLIC_URL}/data.json`))
		.then(res => dispatch({ type: GET_LIST_PEOPLES_SUCCESS, payload: res.data }))
		.catch((err) => {
			if (err.data) {
				dispatch({ type: GET_LIST_PEOPLES_FAIL, payload: err.data });
			} else {
				throw err;
			}
		})
);

export const sortList = param => ({ type: SORT_LIST, payload: param });
export const sortListDirection = param => ({ type: SORT_LIST_DIRECTION, payload: param });
export const toggleFavorite = id => ({ type: TOGGLE_FAVORITE, payload: id });
export const getFromStorageList = () => ({ type: GET_LOCALSTORAGE_LIST });
