import {
	CHANGE_LANG
} from '../constatns/app';

export const toggleLang = lang => ({ type: CHANGE_LANG, payload: lang });
